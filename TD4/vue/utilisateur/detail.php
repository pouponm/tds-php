<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détail de <?php /** @var ModeleUtilisateur $utilisateur */ $utilisateur->getLogin ?></title>
</head>
<body>
<?php
/** @var ModeleUtilisateur $utilisateur */
if($utilisateur!=null){
    echo '<p> Utilisateur ' . $utilisateur->getNom() . ' ' . $utilisateur->getPrenom() . ' de login ' . $utilisateur->getLogin() . '.</p>';
}
else{
    require 'erreur.php';
}
?>
</body>
</html>
