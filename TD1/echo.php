<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
          $prenom = "Marc";

          echo "Bonjour<br> " . $prenom;
          echo "Bonjour<br> $prenom";
          echo 'Bonjour<br> $prenom';

          echo $prenom;
          echo "$prenom<br>";


          $utilisateur1 = array(
                  "prenom" => "Gandalf",
                  "nom" => "Leblanc",
                  "login" => "legrisg"
          );

          $utilisateur2 = array(
                  "prenom" => "Aragorn",
                  "nom" => "Grandpa",
                  "login" => "elessar"
          );

          $utilisateur3 = array(
                  "prenom" => "Manwe",
                  "nom" => "Valar",
                  "login" => "sgndagl"
          );

          $utilisateurs = array();
          $utilisateurs[] = $utilisateur1;
          $utilisateurs[] = $utilisateur2;
          $utilisateurs[] = $utilisateur3;
          var_dump($utilisateurs);

          if(empty($utilisateurs)){
              echo "Il n'y a aucun utilisateur";
          }
          else{
          echo "<ul>";

          foreach ($utilisateurs as $utilisateur) {
              echo "<li>$utilisateur[nom] $utilisateur[prenom]: $utilisateur[login]</li>";
          }

          echo "</ul>";

          };


        ?>
    </body>
</html> 