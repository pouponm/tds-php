<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue ('utilisateur/liste.php',["utilisateurs"=>$utilisateurs]);  //"redirige" vers la vue
    }
    public static function afficherDetail() : void {
        $erreur=0;
        foreach ($_GET as $key => $value) {
            if ($key == 'login') {
                $erreur=1;
            }
        }
        if ($erreur==0) {
            ControleurUtilisateur::afficherVue('utilisateur/erreur.php');
        }
        else{
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
            ControleurUtilisateur::afficherVue ('utilisateur/detail.php',['utilisateur'=>$utilisateur]);  //"redirige" vers la vue
        }
    }

    public static function afficherFormulaireCreation() : void{
        ControleurUtilisateur::afficherVue('utilisateur/formulaireCreation.php');
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisformulaire():void{
                $nom=$_GET['nom'];
                $prenom=$_GET['prenom'];
                $login=$_GET['login'];
                $utilisateur=ModeleUtilisateur::construireDepuisTableauSQL(['nom'=>$nom,'prenom'=>$prenom,'login'=>$login]);
                $utilisateur->ajouter();
                self::afficherListe();
    }
}
?>
