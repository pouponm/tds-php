<?php
include_once "ConnexionBaseDeDonnees.php";
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = substr($login,0,64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string{
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string{
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login,0,64);
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString(): string {
        return "$this->nom $this->prenom: $this->login";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        $utilisateur=new utilisateur($utilisateurFormatTableau['login'],$utilisateurFormatTableau['nom'],$utilisateurFormatTableau['prenom']);
        return $utilisateur;
    }

    public static function recupererUtilisateur():array {
        $listeUtilisateurs=[];
        $pdo=ConnexionBaseDeDonnees::getPdo();
        $pdoStatement=$pdo->query("SELECT * FROM utilisateur");
        foreach($pdoStatement as $utilisateurFormatTableau){
            $listeUtilisateurs[]=self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $listeUtilisateurs;
    }


}
?>

