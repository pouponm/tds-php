<?php
    /** @var Trajet $trajet */
    use App\Covoiturage\Modele\DataObject\Trajet;
?>

<form method="get" action="">
        <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="depart_id">Depart</label> :
            <input type="text" value="<?= htmlspecialchars($trajet->getDepart()) ?>" placeholder="Montpellier" name="depart" id="depart_id" required="">
        </p>
        <p>
            <label for="arrivee_id">Arrivée</label> :
            <input type="text" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" placeholder="Sète" name="arrivee" id="arrivee_id" required="">
        </p>
        <p>
            <label for="date_id">Date</label> :
            <input type="date" value="<?= htmlspecialchars($trajet->getDate()->format("Y-m-d")) ?>" placeholder="JJ/MM/AAAA" name="date" id="date_id" required="">
        </p>
        <p>
            <label for="nbPlaces_id">NbPlaces</label> :
            <input type="int" value="<?= htmlspecialchars($trajet->getNbPlaces()) ?>" placeholder="20" name="nbPlaces" id="nbPlaces_id" required="">
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="int" value="<?= htmlspecialchars($trajet->getPrix()) ?>" placeholder="20" name="prix" id="prix_id" required="">
        </p>
        <p>
            <label for="conducteurLogin_id">Login du conducteur</label> :
            <input type="text" value="<?= htmlspecialchars($trajet->getConducteurLogin()) ?>" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required="">
        </p>
        <p>
            <?php
                if ($trajet->isNonFumeur()) {
                    echo "<label for=\"nonFumeur_id\">Non Fumeur ?</label> :";
                    echo "<input type=\"checkbox\" value=\"1\" placeholder=\"leblancj\" name=\"nonFumeur\" id=\"nonFumeur_id\" checked>";
                }
                else {
                    echo "<label for=\"nonFumeur_id\">Non Fumeur ?</label> :";
                    echo "<input type=\"checkbox\" value=\"0\" placeholder=\"leblancj\" name=\"nonFumeur\" id=\"nonFumeur_id\">";
                }
            ?>
        </p>
        <p>
            <input type="submit" value="Envoyer">
        </p>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='trajet'>
        <input type='hidden' name='id' value="<?= htmlspecialchars($trajet->getId()) ?>">
    </fieldset>
</form>