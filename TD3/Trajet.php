<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $nbPlaces;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $nbPlaces,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $this->recupererPassagers();
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $trajetTableau["date"], // À changer
            $trajetTableau["nbPlaces"],
            $trajetTableau["prix"],
            $trajetTableau["conducteurLogin"], // À changer
            $trajetTableau["nonFumeur"], // À changer ?
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNbPlaces(): int
    {
        return $this->nbPlaces;
    }

    public function setNbPlaces(int $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        $retour = "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}) (passagers:";
        foreach ($this->passagers as $passager) {
            $retour .= "$passager, ";
        }
        $retour .= ").</p>";
        return $retour;
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajetFormatTableau["date"]=new DateTime($trajetFormatTableau["date"]);
            $trajetFormatTableau["conducteurLogin"]=Utilisateur::recupererUtilisateurParLogin($trajetFormatTableau["conducteurLogin"]);
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public static function ajouter($trajet){
        $sql = "INSERT INTO trajet (depart, arrivee, date, nbPlaces, prix, conducteurLogin, nonFumeur) VALUES (:departTag, :arriveeTag, :dateTag, :nbPlacesTag, :prixTag, :conducteurLoginTag, :nonFumeurTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "departTag" => $trajet->depart,
            "arriveeTag" => $trajet->arrivee,
            "dateTag" => $trajet->date->format("Y-m-d"),
            "nbPlacesTag" => $trajet->nbPlaces,
            "prixTag" => $trajet->prix,
            "conducteurLoginTag" => $trajet->conducteur->getLogin(),
        );
        if ($trajet->nonFumeur){
            $values["nonFumeurTag"] = 1;
        }
        else{
            $values["nonFumeurTag"] = 0;
        }
        $pdoStatement->execute($values);
    }

    private function recupererPassagers() : array {
        $sql = "SELECT passagerLogin FROM passager WHERE trajetId = :trajetIdTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);
        $values = array(
            "trajetIdTag" => $this->id
        );
        $pdoStatement -> execute($values);
        $passagers = [];
        foreach($pdoStatement as $passagerFormatTableau) {
            $passagers[] = Utilisateur::recupererUtilisateurParLogin($passagerFormatTableau["passagerLogin"]);
        }
        return $passagers;
    }
}