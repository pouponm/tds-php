<?php
        /** @var Utilisateur $utilisateur */
        use App\Covoiturage\Modele\DataObject\Utilisateur;
        echo "<p> Utilisateur de login " . htmlspecialchars($utilisateur->getLogin()) . " et de nom " . htmlspecialchars($utilisateur->getNom()) . " et de prenom " . htmlspecialchars($utilisateur->getPrenom()) . "</p>";
?>