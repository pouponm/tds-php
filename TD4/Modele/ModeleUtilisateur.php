<?php

include_once "ConnexionBaseDeDonnees.php";

class ModeleUtilisateur
{

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string
    {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login, 0, 64);
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    //public function __toString(): string
    //{
    //    return "$this->nom $this->prenom: $this->login";
    //}

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur
    {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
        return $utilisateur;
    }

    public static function recupererUtilisateurs(): array
    {
        $listeUtilisateurs = [];
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $listeUtilisateurs[] = self::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $listeUtilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if(!$utilisateurFormatTableau) return null;
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void{
        $sql = "INSERT INTO utilisateur (nom,prenom,login) VALUES (:nomTag, :prenomTag, :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
            "loginTag" => $this->login,
        );
        $pdoStatement->execute($values);
    }



}

?>

