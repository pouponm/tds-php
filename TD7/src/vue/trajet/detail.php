<?php
    /** @var Trajet $trajet */
    use App\Covoiturage\Modele\DataObject\Trajet;
    echo "<p> Trajet de id " . htmlspecialchars($trajet->getId()) . " et de depart " . htmlspecialchars($trajet->getDepart()) . " et d'arrivée " . htmlspecialchars($trajet->getArrivee()) . " le " . htmlspecialchars($trajet->getDate()->format('d-m-Y')) . "</p>";
    echo "<p> " . htmlspecialchars($trajet->getNbPlaces()) . " places pour la somme de " . htmlspecialchars($trajet->getPrix()) . "€ l'unité</p>";
    echo "<p> Conducteur " . htmlspecialchars($trajet->getConducteur()) . "</p>";
?>