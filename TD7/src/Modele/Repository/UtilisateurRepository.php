<?php
    namespace App\Covoiturage\Modele\Repository;

    use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
    use App\Covoiturage\Modele\ModeleTrajet;
    use App\Covoiturage\Modele\DataObject\Utilisateur;
    use App\Covoiturage\Modele\Repository\AbstractRepository;
    use App\Covoiturage\Modele\DataObject\AbstractDataObject;

    class UtilisateurRepository extends AbstractRepository {

        public function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
        {
            return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
        }

        public static function recupererTrajetsCommePassager(Utilisateur $user): array
        {
            $sql = "SELECT id, depart, arrivee, date, nbPlaces, prix, conducteurLogin, nonFumeur FROM passager JOIN trajet ON passager.trajetid = trajet.id WHERE passagerLogin = :logintag";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = array(
                "logintag" => $user->getLogin(),
            );
            $pdoStatement->execute($values);
            $trajets = [];
            foreach ($pdoStatement as $trajetFormatTableau) {
                $trajets[] = ModeleTrajet::recupererTrajetParId($trajetFormatTableau["id"]);
            }
            return $trajets;
        }

        protected function getNomTable(): string {
            return "utilisateur";
        }

        protected function getNomClePrimaire() : string {
            return "login";
        }

        protected function getNomsColonnes(): array {
            return ["login", "nom", "prenom"];
        }

        protected function formatTableauSQL(AbstractDataObject $objet): array
        {
            /** @var Utilisateur $objet */
            return array(
                "logintag" => $objet->getLogin(),
                "nomtag" => $objet->getNom(),
                "prenomtag" => $objet->getPrenom(),
            );
        }
    }
?>