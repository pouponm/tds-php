<?php
    namespace App\Covoiturage\Modele\Repository;
    use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
    use App\Covoiturage\Modele\DataObject\AbstractDataObject;
    use App\Covoiturage\Modele\DataObject\Utilisateur;
    use App\Covoiturage\Modele\DataObject\Trajet;
    use DateMalformedStringException;
    use DateTime;

    class TrajetRepository extends AbstractRepository {

        /**
         * @throws DateMalformedStringException
         */
        public function construireDepuisTableauSQL(array $trajetFormatTableau) : Trajet {
            $passager = [];
            $trajet = new Trajet(
                $trajetFormatTableau["id"],
                $trajetFormatTableau["depart"],
                $trajetFormatTableau["arrivee"],
                new DateTime($trajetFormatTableau["date"]), // À changer
                $trajetFormatTableau["nbPlaces"],
                $trajetFormatTableau["prix"],
                (new UtilisateurRepository())->recupererParClePrimaire($trajetFormatTableau["conducteurLogin"]), // À changer
                $trajetFormatTableau["nonFumeur"],// À changer ?
                $passager
            );
            $trajet->setPassagers(TrajetRepository::recupererPassagers($trajet));
            return $trajet;
        }

        /**
         * @return Trajet[]

        public static function recupererTrajets() : array {
            $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

            $trajets = [];
            foreach($pdoStatement as $trajetFormatTableau) {
                $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
            }

            return $trajets;
        }*/

        /**
         * @return Utilisateur[]
         */
        public static function recupererPassagers(Trajet $trajet): array {
            $sql = "SELECT passagerLogin FROM passager WHERE trajetId = :trajetIdTag";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);
            $values = array(
                "trajetIdTag" => $trajet->getId()
            );
            $pdoStatement -> execute($values);
            $passagers = [];
            foreach($pdoStatement as $passagerFormatTableau) {
                $passagers[] = (new UtilisateurRepository())->recupererParClePrimaire($passagerFormatTableau["passagerLogin"]);
            }
            return $passagers;
        }

        protected function getNomTable(): string {
            return "trajet";
        }

        protected function getNomClePrimaire() : string {
            return "id";
        }

        protected function getNomsColonnes(): array {
            return ["id", "depart", "arrivee", "date", "nbPlaces", "prix",	"conducteurLogin", "nonFumeur"];
        }

        protected function formatTableauSQL(AbstractDataObject $objet): array {
            /** @var Trajet $objet */
            $fumeur = 0;
            if ($objet->isNonFumeur()): $fumeur = 1;
            endif;
            return array(
                "idtag" => $objet->getId(),
                "departtag" => $objet->getDepart(),
                "arriveetag" => $objet->getArrivee(),
                "datetag" => $objet->getDate()->format('Y-m-d'),
                "nbPlacestag" => $objet->getNbPlaces(),
                "prixtag" => $objet->getPrix(),
                "conducteurLogintag" => $objet->getConducteur()->getLogin(),
                "nonFumeurtag" => $fumeur,
            );
        }
    }
?>