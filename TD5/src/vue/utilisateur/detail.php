
<?php
/** @var ModeleUtilisateur $utilisateur */
if($utilisateur!=null){
    echo '<p> Utilisateur ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login ' . htmlspecialchars($utilisateur->getLogin()) . '.</p>';
}
else{
    require_once __DIR__ . '/erreur.php';
}
?>
