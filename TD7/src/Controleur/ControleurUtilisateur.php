<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

require_once __DIR__ . "/../Modele/DataObject/Utilisateur.php"; // chargement du modèle
class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();//appel au modèle pour gérer la BD
        self::afficherVue("vueGenerale.php", ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
    }

    public static function afficherDetail(): void
    {
        $erreur = 0;
        foreach ($_GET as $key => $value) {
            if ($key == "login") {
                $erreur = 0;
                $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($value);
                if ($utilisateur == null) $erreur = 1;
                else self::afficherVue("vueGenerale.php", ["utilisateur" => $utilisateur, "titre" => "Détail d'un utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
            } else $erreur = 1;
        }
        if ($erreur == 1) {
            $messageErreur = "Error user : " . $value . " not defined";
            self::afficherErreur($messageErreur);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue("vueGenerale.php" , ["titre" => "Formulaire création utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $newutilisateur = self::construireDepuisFormulaire($_GET);
        if ((new UtilisateurRepository)->ajouter($newutilisateur)) {
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue("vueGenerale.php", ["titre" => "utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
        }
        else {
            self::afficherErreur("erreur: impossible de créer l'utilisateur");
        }
    }

    public static function supprimer(): void {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["titre" => "utilisateur supprimé", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "utilisateurs" => $utilisateurs]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVue("vueGenerale.php", ["titre" => "formulaire mise a jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
    }

    public static function mettreAJour(): void {
        $newutilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($newutilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue("vueGenerale.php", ["titre" => "utilisateur mis a jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "utilisateurs" => $utilisateurs]);
    }

    public static function afficherErreur(string $messageErreur): void {
        self::afficherVue("vueGenerale.php", ["titre" => "error page", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        return new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
    }

}

?>