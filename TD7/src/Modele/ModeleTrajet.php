<?php
namespace App\Covoiturage\Modele;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/src/Modele/Utilisateur.php';

class ModeleTrajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $nbPlaces;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;

    public function __construct(
        ?int        $id,
        string      $depart,
        string      $arrivee,
        DateTime    $date,
        int         $nbPlaces,
        int         $prix,
        Utilisateur $conducteur,
        bool        $nonFumeur,
        array       $passagers
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    /**
     * @throws DateMalformedStringException
     */
    public static function construireDepuisTableauSQL(array $trajetTableau) : ModeleTrajet {
        $passager = [];
        $trajet = new ModeleTrajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["nbPlaces"],
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"],// À changer ?
            $passager
        );
        $trajet->setPassagers($trajet->recupererPassagers());
        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function setNbPlaces(int $nbPlaces): void {
        $this->nbPlaces = $nbPlaces;
    }

    public function getNbPlaces(): int {
        return $this->nbPlaces;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void {
        $this->passagers = $passagers;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return ModeleTrajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = ModeleTrajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : void {
        $sql = "INSERT INTO trajet (depart, arrivee, date, nbPlaces, prix, conducteurLogin, nonFumeur) VALUES (:departtag, :arriveetag, :datetag, :nbPlacestag, :prixtag, :conducteurLogintag, :nonFumeurtag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);
        if ($this->nonFumeur) $fumer = 1;
        else $fumer = 0;
        $values = array(
            "departtag" => $this->depart,
            "arriveetag" => $this->arrivee,
            "datetag" => $this->date->format("Y-m-d"),
            "nbPlacestag" => $this->nbPlaces,
            "prixtag" => $this->prix,
            "conducteurLogintag" => $this->conducteur->getLogin(),
            "nonFumeurtag" => $fumer
        );
        $pdoStatement -> execute($values);
    }

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array {
        $sql = "SELECT passagerLogin FROM passager WHERE trajetId = :trajetIdTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo() -> prepare($sql);
        $values = array(
            "trajetIdTag" => $this->id
        );
        $pdoStatement -> execute($values);
        $passagers = [];
        foreach($pdoStatement as $passagerFormatTableau) {
            $passagers[] = (new UtilisateurRepository())->recupererParClePrimaire($passagerFormatTableau["passagerLogin"]);
        }
        return $passagers;
    }

    /**
     * @throws DateMalformedStringException
     */
    public static function recupererTrajetParId (string $id) : ModeleTrajet {
        $sql = "SELECT * from trajet WHERE id = :idtag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idtag" => $id,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $trajetFormatTableau = $pdoStatement->fetch();
        return ModeleTrajet::construireDepuisTableauSQL($trajetFormatTableau);
    }
}
