<?php
    namespace App\Covoiturage\Modele\Repository;
    use App\Covoiturage\Modele\ConnexionBaseDeDonnees;
    use App\Covoiturage\Modele\DataObject\AbstractDataObject;
    use App\Covoiturage\Modele\DataObject\Utilisateur;

    abstract class AbstractRepository {
        public function mettreAJour(AbstractDataObject $objet): void
        {
            $tab = $this->getNomsColonnes($objet);
            $str = " ";
            foreach ($tab as $value) {
                $str = $str . $value . " = :" . $value . "tag";
                if ($value != end($tab)): $str = $str .  ", ";
                endif;
            }
            $sql = "UPDATE " . $this->getNomTable() . " SET " . $str . " WHERE " . $tab[0] . " = :" . $tab[0] . "tag";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = $this->formatTableauSQL($objet);
            $pdoStatement->execute($values);
        }

        public function ajouter(AbstractDataObject $objet): bool
        {
            $sql = "INSERT INTO " . $this->getNomTable() . " (" . join(", ", $this->getNomsColonnes()) . ") VALUES (:" . join("tag, :", $this->getNomsColonnes()) . "tag)";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = $this->formatTableauSQL($objet);
            return $pdoStatement->execute($values);
        }

        public function supprimer($valeurClePrimaire): void
        {
            $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :loginTag";
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
            $values = array(
                "loginTag" => $valeurClePrimaire,
            );
            $pdoStatement->execute($values);
        }

        public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject
        {
            $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :loginTag";
            // Préparation de la requête
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            $values = array(
                "loginTag" => $clePrimaire,
                //nomdutag => valeur, ...
            );
            // On donne les valeurs et on exécute la requête
            $pdoStatement->execute($values);

            // On récupère les résultats comme précédemment
            // Note: fetch() renvoie false si pas d'utilisateur correspondant
            $objetFormatTableau = $pdoStatement->fetch();
            if (!$objetFormatTableau) return null;
            return $this->construireDepuisTableauSQL($objetFormatTableau);
        }

        public function recuperer(): array
        {
            $tableau = [];
            $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM " . $this->getNomTable());
            foreach ($pdoStatement as $utilisateurFormatTableau) {
                $tableau[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
            }
            return $tableau;
        }

        protected abstract function getNomTable(): string;

        protected abstract function getNomClePrimaire() : string;

        protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

        protected abstract function getNomsColonnes(): array;

        protected abstract function formatTableauSQL(AbstractDataObject $objet): array;
    }
?>