<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur as ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue ('vueGenerale.php',["utilisateurs"=>$utilisateurs,"titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }
    public static function afficherDetail() : void {
        $erreur=0;
        foreach ($_GET as $key => $value) {
            if ($key == 'login') {
                $erreur=1;
            }
        }
        if ($erreur==0) {
            ControleurUtilisateur::afficherVue('vueGenerale.php',["titre" => "Erreur", "cheminCorpsVue" =>'utilisateur/erreur.php']);
        }
        else{
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']); //appel au modèle pour gérer la BD
            ControleurUtilisateur::afficherVue ('vueGenerale.php',['utilisateur'=>$utilisateur,"titre" => "Detail de l'utilisateur", "cheminCorpsVue" =>'utilisateur/detail.php']);  //"redirige" vers la vue
        }
    }

    public static function afficherFormulaireCreation() : void{
        ControleurUtilisateur::afficherVue('vueGenerale.php',["titre" => "Formulaire de création", "cheminCorpsVue" =>'utilisateur/formulaireCreation.php']);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function creerDepuisformulaire():void{
                $nom=$_GET['nom'];
                $prenom=$_GET['prenom'];
                $login=$_GET['login'];
                $utilisateur=ModeleUtilisateur::construireDepuisTableauSQL(['nom'=>$nom,'prenom'=>$prenom,'login'=>$login]);
                $utilisateur->ajouter();
                self::afficherListe();
    }
}
?>
